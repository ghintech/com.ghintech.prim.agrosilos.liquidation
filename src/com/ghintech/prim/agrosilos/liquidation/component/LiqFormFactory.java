package com.ghintech.prim.agrosilos.liquidation.component;

import java.util.logging.Level;

import org.adempiere.webui.factory.IFormFactory;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.IFormController;
import org.compiere.util.CLogger;

public class LiqFormFactory implements IFormFactory{
	protected transient CLogger log = CLogger.getCLogger(this.getClass());

	@Override
	public ADForm newFormInstance(String formName) {
		if (formName.startsWith("org.sg.form.VLiquidation")){
            Object form = null;
            Class<?> clazz = null;
            ClassLoader loader = this.getClass().getClassLoader();
            try {
                clazz = loader.loadClass(formName);
            }
            catch (Exception exception) {
                this.log.log(Level.FINE, "Error al cargar form");
            }
            if (clazz != null) {
                try {
                    form = clazz.getDeclaredConstructor().newInstance();
                }
                catch (Exception exception) {
                    this.log.log(Level.FINE, "Error al cargar form");
                }
            }
            if (form != null) {
                if (form instanceof ADForm) {
                	return (ADForm) form;
                }
                if (form instanceof IFormController) {
                	IFormController controller = (IFormController) form;
                    ADForm adForm = controller.getForm();
                    adForm.setICustomForm(controller);
                    return adForm;
                }
            }
        }
		return null;
	}
}
