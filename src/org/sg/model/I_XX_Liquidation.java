/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.sg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for XX_Liquidation
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_XX_Liquidation 
{

    /** TableName=XX_Liquidation */
    public static final String Table_Name = "XX_Liquidation";

    /** AD_Table_ID=1000027 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Charge_ID */
    public static final String COLUMNNAME_C_Charge_ID = "C_Charge_ID";

	/** Set Charge.
	  * Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID);

	/** Get Charge.
	  * Additional document charges
	  */
	public int getC_Charge_ID();

	public I_C_Charge getC_Charge() throws RuntimeException;

    /** Column name C_Invoice_ID */
    public static final String COLUMNNAME_C_Invoice_ID = "C_Invoice_ID";

	/** Set Invoice.
	  * Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID);

	/** Get Invoice.
	  * Invoice Identifier
	  */
	public int getC_Invoice_ID();

	public I_C_Invoice getC_Invoice() throws RuntimeException;

    /** Column name C_PaySelection_ID */
    public static final String COLUMNNAME_C_PaySelection_ID = "C_PaySelection_ID";

	/** Set Payment Selection.
	  * Payment Selection
	  */
	public void setC_PaySelection_ID (int C_PaySelection_ID);

	/** Get Payment Selection.
	  * Payment Selection
	  */
	public int getC_PaySelection_ID();

	public I_C_PaySelection getC_PaySelection() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name GrandTotal */
    public static final String COLUMNNAME_GrandTotal = "GrandTotal";

	/** Set Grand Total.
	  * Total amount of document
	  */
	public void setGrandTotal (BigDecimal GrandTotal);

	/** Get Grand Total.
	  * Total amount of document
	  */
	public BigDecimal getGrandTotal();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name TotalLines */
    public static final String COLUMNNAME_TotalLines = "TotalLines";

	/** Set Total Lines.
	  * Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines);

	/** Get Total Lines.
	  * Total of all document lines
	  */
	public BigDecimal getTotalLines();

    /** Column name TotalWeight */
    public static final String COLUMNNAME_TotalWeight = "TotalWeight";

	/** Set TotalWeight	  */
	public void setTotalWeight (BigDecimal TotalWeight);

	/** Get TotalWeight	  */
	public BigDecimal getTotalWeight();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name XX_Cancel_Liquidation */
    public static final String COLUMNNAME_XX_Cancel_Liquidation = "XX_Cancel_Liquidation";

	/** Set Cancel Liquidation	  */
	public void setXX_Cancel_Liquidation (String XX_Cancel_Liquidation);

	/** Get Cancel Liquidation	  */
	public String getXX_Cancel_Liquidation();

    /** Column name XX_CreditMemo_ID */
    public static final String COLUMNNAME_XX_CreditMemo_ID = "XX_CreditMemo_ID";

	/** Set Credit Memo	  */
	public void setXX_CreditMemo_ID (int XX_CreditMemo_ID);

	/** Get Credit Memo	  */
	public int getXX_CreditMemo_ID();

	public I_C_Invoice getXX_CreditMemo() throws RuntimeException;

    /** Column name XX_Cuota_Arrocera */
    public static final String COLUMNNAME_XX_Cuota_Arrocera = "XX_Cuota_Arrocera";

	/** Set Cuota Arrocera	  */
	public void setXX_Cuota_Arrocera (BigDecimal XX_Cuota_Arrocera);

	/** Get Cuota Arrocera	  */
	public BigDecimal getXX_Cuota_Arrocera();

    /** Column name XX_DebitMemo_ID */
    public static final String COLUMNNAME_XX_DebitMemo_ID = "XX_DebitMemo_ID";

	/** Set Debit Memo	  */
	public void setXX_DebitMemo_ID (int XX_DebitMemo_ID);

	/** Get Debit Memo	  */
	public int getXX_DebitMemo_ID();

	public I_C_Invoice getXX_DebitMemo() throws RuntimeException;

    /** Column name XX_Liquidation_ID */
    public static final String COLUMNNAME_XX_Liquidation_ID = "XX_Liquidation_ID";

	/** Set Liquidation	  */
	public void setXX_Liquidation_ID (int XX_Liquidation_ID);

	/** Get Liquidation	  */
	public int getXX_Liquidation_ID();

    /** Column name XX_QuoteAmt */
    public static final String COLUMNNAME_XX_QuoteAmt = "XX_QuoteAmt";

	/** Set Quote Amount	  */
	public void setXX_QuoteAmt (BigDecimal XX_QuoteAmt);

	/** Get Quote Amount	  */
	public BigDecimal getXX_QuoteAmt();
}
